import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs'; 

import { Login, LoginMessageInfo, MessageInfo } from '../login';
import { BppService } from '../bpp.service';   

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  @Input() login:  Login = new Login; 

  //login:Login = new Login;
  messageInfo:MessageInfo;
  firstName: string;

  constructor(private bppService: BppService) { 
    
    // this.login.username = 'brian.hahn@alliedbuilding.com'; 
    // this.login.password = 'ALLied1234'; 
    // this.login.siteId = 'homeSite'; 

  }

  ngOnInit() {

    // this.bppService.tryLogin(this.login).subscribe((data: HttpResponse<LoginMessageInfo>) => {
    //   console.log(data);
    //   this.messageInfo = data["messageInfo"];
    //   this.firstName = this.messageInfo.firstName; 
  }

  tryLogin(): void {
    this.login.siteId = 'homeSite';
    this.bppService.tryLogin(this.login).subscribe((resp: HttpResponse<LoginMessageInfo>) => {
      console.log(resp);
      this.messageInfo = resp["messageInfo"];
      this.firstName = this.messageInfo.firstName; 
    }); 
  }

  tryLogout(): void {
    this.bppService.tryLogout(this.login).subscribe((resp: HttpResponse<any>) => {
      console.log(resp)
      this.firstName = null; 
    })
  
  }
  

}
