export class Login{
    username: string; 
    password: string; 
    siteId: string; 
}

export class LoginMessageInfo{
    messageInfo:MessageInfo; 
}

export class MessageInfo{
    accountBranch: AccountBranch;
    lastName:string; 
    cartLineItems:number; 
    profileId:string;
    emailAddress:string;
    firstName:string; 
    lastSelectedAccount:Account  
}

export class Account{
    accountName:string; 
    accountEnabled:boolean; 
    accountLegacyId:string; 
    accountViewPrices:boolean; 
}

export class AccountBranch{
    address:Address; 
    branchNumber:string; 
    branchName:string;
    branchPhone:string; 
    branchRegionId:string; 
}
export class Address{
    postalCode:string; 
    state:string; 
    address1:string; 
    address2:string;
    address3:string; 
    country:string; 
    city:string; 
}