import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'; 
import { Observable, of } from 'rxjs'; 
import { map ,catchError, tap} from 'rxjs/operators'; 

import { Login, LoginMessageInfo } from './login'; 

const endpoint = 'https://beacon-uat.becn.com/rest/model/REST/NewRestService/v1/rest/com/becn/'; 
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}; 

@Injectable({
  providedIn: 'root'
})

export class BppService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res; 
    return body || {}; 
  }

  tryLogin (login): Observable<any> {
    console.log(endpoint + 'login , ' + JSON.stringify(login));
    return this.http.post<any>(endpoint + 'login', JSON.stringify(login), httpOptions)
      .pipe(
        tap(()=> console.debug(`Attempted to log in with username=${login.username}`)), 
        map(this.extractData), //not sure what this actually is supposed to do. 
        catchError(this.handleError<any>('tryLogin'))
      ); 
  }

  tryLogout (login): Observable<any> {
    console.log(endpoint + 'logout , ' + JSON.stringify(login)); 
    return this.http.post<any>(endpoint + 'logout', JSON.stringify(login), httpOptions)
      .pipe(
        catchError(this.handleError<any>('tryLogout'))
      ); 
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); 

      return of(result as T); 
    }; 
  }

}
